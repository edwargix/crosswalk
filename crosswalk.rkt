#!/usr/bin/env racket
#lang racket
(require data/heap)
(require data/queue)
(require racket/contract)
(require "welford.rkt")

; command-line arguments ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define args
  (command-line
   #:args
   (N uniform-file-1 uniform-file-2 uniform-file-3)
   (list N uniform-file-1 uniform-file-2 uniform-file-3)))

(define N-peds (string->number (car args)))
(define N-autos (string->number (car args)))
(define automobile-arrivals-file (open-input-file (cadr args)))
(define pedestrian-arrivals-file (open-input-file (caddr args)))
(define crosswalk-button-file (open-input-file (cadddr args)))

; statistical distributions ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (exponential μ uniform-file)
  (* (- μ) (log (read uniform-file))))

(define (uniform a b uniform-file)
  (+ a (* (- b a) (read uniform-file))))

(define (bernoulli p uniform-file)
  (if (< (read uniform-file) (- 1 p)) 0 1))

; utility functions ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (mph->ft/s mph)
  (/ (* mph 5280) 3600))

(define (dir? d)
  (or (eq? d 'west) (eq? d 'east)))

; events ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; datastructure
(struct event (at form) #:transparent)
; how to order the events in the event-list
(define (event<=? x y)
  (<= (event-at x) (event-at y)))
; the actual event-list; even thoug it's not actually a list (but a heap), the
; convention is to call it event-list
(define event-list (make-heap event<=?))
(define (new-event at form)
  (let ([e (event at form)])
    (heap-add! event-list e)
    e))

; constants ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define B 330) ; width of residential block
(define w 24) ; width of crosswalk
(define S 46) ; width of street
(define red-time 18) ; time traffic signal is red
(define yellow-time 8) ; time traffic signal is yellow
(define green-time 35) ; minimum time traffic signal is green
(define ir_p (* 60 (/ 1 (add1 3)))) ; interarrival-rate of peds
(define ir_a (* 60 (/ 1 (add1 4)))) ; interarrival-rate of autos
(define L 9) ; length of an automobile
(define α 10) ; acceleration of auto; -α for deceleration

; simulation clock ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define t 0)

; safety signal ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define light
  'green)

(define (can-walk?)
  (eq? light 'red))

(define green-timer-expired #t) ; set to true so that first button press causes
                                ; yellow

(define btn-pushed #f)

(define red-expires-time 0)

(define (walk-time-left)
  (if (eq? light 'red)
      (- red-expires-time t)
      0))

(define (yellow-begins)
  (set! light 'yellow)
  (new-event (+ t yellow-time) '(yellow-expires)))

(define (impatient-ped-event? ev)
  ; this is kind of a hack
  (eq? (car (event-form ev)) 'button-pushed))

(define (yellow-expires)
  (set! light 'red)
  (let ([ev (new-event (+ t red-time) '(red-expires))])
    (set! red-expires-time (event-at ev)))
  (set! active-autos
        (let filter-out-delayed ([autos active-autos])
          (if (empty? autos)
              empty
              (let ([a (car autos)])
                (if (<= (auto-leave-crosswalk-time a) t)
                    (begin
                      (welford-add-data auto-delays 0)
                      (filter-out-delayed (cdr autos)))
                    (if (< (auto-reach-crosswalk-time a) red-expires-time)
                        (begin
                          (welford-add-data auto-delays (calculate-auto-delay a))
                          (filter-out-delayed (cdr autos)))
                        (cons a (filter-out-delayed (cdr autos)))))))))
  (set! count-crossed 0)
  ; remove events of peds waiting to hit button
  ; let waiting pedestrians cross
  (queue-filter! waiting-peds
                 (λ (p)
                   (if (and (< count-crossed 20))
                       (if (< (walk-time-left) (/ S (ped-speed p)))
                           ; this should never happen
                           (error "pedestrian forever waiting")
                           (begin
                             (ped-go p)
                             #f))
                       #t)))
  (for (#:when (and (< count-crossed 20) (non-empty-queue? waiting-peds)))
    (let ([p (dequeue! waiting-peds)])
      (when (< (walk-time-left) (/ S (ped-speed p)))
        ; this should never happen
        (error "pedestrian forever waiting"))
      (ped-go p))))

(define (red-expires)
  (set! light 'green)
  ; peds that couldn't make it may push that button right away (6c)
  (for ([_ (in-range (queue-length waiting-peds))])
    (when (bernoulli (/ 15 16) crosswalk-button-file)
      (button-pushed)))
  ; peds will push the button after 1 minute of waiting due to NO WALK (6b)
  (when (not (queue-empty? waiting-peds))
    (new-event (+ t 60) '(button-pushed)))
  (new-event (+ t green-time)
             '(if btn-pushed
                  (begin
                    (yellow-begins)
                    (set! btn-pushed #f))
                  (set! green-timer-expired #t))))

(define (button-pushed)
  (when (eq? light 'green)
    (if green-timer-expired
        (begin
          (yellow-begins)
          (set! green-timer-expired #f))
        (set! btn-pushed #t))))

; pedestrians ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(struct ped (arrival-time speed dir) #:transparent)

(define ped-delays (make-welford))

(define (new-ped dir)
  (ped (+ t (exponential ir_p pedestrian-arrivals-file))
       (uniform 2.6 4.1 pedestrian-arrivals-file)
       dir))

(define (ped-min-delay p)
  (/ (+ B S S) (ped-speed p)))

(define (ped-go p)
  (welford-add-data ped-delays (- (- (+ t (/ S (ped-speed p)))
                                     (ped-arrival-time p))
                                  (ped-min-delay p)))
  (set! count-crossed (add1 count-crossed)))

(define count-crossed 0)

(define waiting-peds (make-queue))
(define (ped-at-button p)
  (if (and (>= (walk-time-left) (/ S (ped-speed p)))
           (< count-crossed 20))
      (ped-go p)
      (if (can-walk?)
          ; signal is WALK but this ped can't make it, either because 20 peds
          ; have crossed or the ped's not fast enough. There's no need to start
          ; the timer for 6b because that only happens during a NO WALK signal
          (enqueue! waiting-peds p)
          ; signal is NO WALK
          (begin
            ; ped may push button, based on number of peds waiting already (6a)
            (when (bernoulli (match (queue-length waiting-peds)
                               [0 (/ 15 16)]
                               [n (/ 1 (add1 n))])
                             crosswalk-button-file)
              (button-pushed))
            ; since NO WALK, start timer for this ped to push button after 1
            ; minute (6b)
            (new-event (+ t 60) '(button-pushed))
            (enqueue! waiting-peds p)))))

(define (ped-arrival p)
  ;; event for ped getting to button
  (new-event (+ t (/ (+ B S) (ped-speed p))) `(ped-at-button ,p))
  ;; event for next ped arrival
  (when (> N-peds 0)
    (let ([new-p (new-ped (ped-dir p))])
      (new-event
       (ped-arrival-time new-p)
       `(ped-arrival ,new-p)))
    (set! N-peds (sub1 N-peds))))

; autos ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(struct auto (arrival-time speed dir reach-crosswalk-time leave-crosswalk-time)
  #:transparent)

(define active-autos '())
(define auto-delays (make-welford))

(define (new-auto dir)
  (let ([speed (mph->ft/s (uniform 25 35 automobile-arrivals-file))]
        [arrival (+ t (exponential ir_a automobile-arrivals-file))])
    (auto arrival
          speed
          dir
          (+ arrival (/ (+ (* B (/ 7 2)) (* S 3) (- (/ w 2))) speed))
          (+ arrival (/ (+ (* B (/ 7 2)) (* S 3) w L) speed)))))

(define (auto-min-delay a)
  (/ (+ (* 7 B) (* 6 S)) (auto-speed a)))

(define (auto-arrival a)
  (-> auto? any)
  (if (and (eq? light 'red)
           (< (auto-reach-crosswalk-time a) red-expires-time))
      (welford-add-data auto-delays (calculate-auto-delay a))
      (set! active-autos (cons a active-autos)))
  ;; event for next auto arrival
  (when (> N-autos 0)
    (let ([new-a (new-auto (auto-dir a))])
      (new-event
       (auto-arrival-time new-a)
       `(auto-arrival ,new-a)))
    (set! N-autos (sub1 N-autos))))

(define (calculate-auto-delay a)
  (let* ([v_j (auto-speed a)]
         [b_j (/ (expt v_j 2) (* 2 α))]
         [t_j (/ v_j α)]
         [e_j (auto-arrival-time a)])
    (- (+ (/ (+ (* 7 B) (* 6 S) (* -2 b_j))
             v_j)
          (* 2 t_j)
          (- red-expires-time
             (+ e_j
                (/ (+ (* B (/ 7 2))
                      (* 3 S)
                      (- (/ w 2))
                      (- b_j))
                   v_j)
                t_j)))
       (auto-min-delay a))))

; initial events ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(for ([d '(west east)])
  (heap-add! event-list (let ([p (new-ped d)])
                          (event (ped-arrival-time p)
                                 `(ped-arrival ,p))))
  (heap-add! event-list (let ([a (new-auto d)])
                          (event (auto-arrival-time a)
                                 `(auto-arrival ,a)))))

; execute events ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define-namespace-anchor nsa)
(define ns (namespace-anchor->namespace nsa))

(define (run-event)
  (let ([ev (heap-min event-list)])
    (heap-remove-min! event-list)
    (set! t (event-at ev))
    (eval (event-form ev) ns))
  (when (> (heap-count event-list) 0)
    (run-event)))

(run-event)

; all autos at this point won't be delayed ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(for ([_ (in-range (length active-autos))])
  (welford-add-data auto-delays 0))

; print results ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(displayln (format "OUTPUT ~a ~a ~a"
                   (welford-mean auto-delays)
                   (welford-variance auto-delays)
                   (welford-mean ped-delays)))

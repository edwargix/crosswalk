all:
	@echo "Voilà"

crosswalk.tar.gz: SIM crosswalk.rkt welford.rkt
	tar czvf $@ $^

upload: crosswalk.tar.gz run.sh
	scp crosswalksim-grader.tar.bz2 davidflorness@isengard:
	scp crosswalk.tar.gz davidflorness@isengard:
	ssh davidflorness@isengard 'bash -s' < run.sh

clean: crosswalk.tar.gz
	rm -rf $^

submit: SIM crosswalk.rkt welford.rkt doc/README Makefile
	git archive -o davidflorness.tar.gz HEAD $^

.PHONY: upload clean submit

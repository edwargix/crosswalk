#!/bin/bash
rm -rf crosswalk
mkdir crosswalk
mv crosswalk.tar.gz crosswalk
mv crosswalksim-grader.tar.bz2 crosswalk
pushd crosswalk
tar xvzf crosswalk.tar.gz
tar xvjf crosswalksim-grader.tar.bz2
source ~khellman/SIMGRADING/setup.sh ~khellman/SIMGRADING
./Crosswalk-simple/grader.sh
popd
